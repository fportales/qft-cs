# Quantum Field Theory in Curved Spacetimes

This is a report I'm making for my Ph.D. I'm using the Tufte-Book class.

## Structure
* `report.tex` is the main file. Compile this file directly. I'd recommend `pdflatex`.
* `QFTinCS.bib` is the bibliography.
* `content/introduction.tex` is the Introduction chapter.
